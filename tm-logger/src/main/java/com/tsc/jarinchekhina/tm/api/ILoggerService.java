package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.dto.EntityLogDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

/**
 * ILoggerService
 *
 * @author Yuliya Arinchekhina
 */
public interface ILoggerService {

    void log(@NotNull String jsonEntity);

}
