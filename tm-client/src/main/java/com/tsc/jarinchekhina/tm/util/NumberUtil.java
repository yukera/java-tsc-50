package com.tsc.jarinchekhina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class NumberUtil {

    @NotNull
    private static final String[] Q = new String[]{"", "K", "M", "G", "T", "P", "E"};

    @NotNull
    public String formatBytes(long bytes) {
        for (int i = 6; i > 0; i--) {
            double step = Math.pow(1024, i);
            if (bytes > step) return String.format("%3.1f %s", bytes / step, Q[i] + "B");
        }
        return Long.toString(bytes) + "B";
    }

}
