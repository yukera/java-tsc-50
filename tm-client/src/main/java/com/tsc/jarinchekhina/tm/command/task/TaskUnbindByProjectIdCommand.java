package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUnbindByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "unbind task from project by id";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().unbindTaskByProjectId(serviceLocator.getSession(), taskId);
    }

}