package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static SessionDTO session;

    @BeforeClass
    public static void before() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
        Assert.assertNotNull(session);
    }

    @AfterClass
    public static void after() {
        bootstrap.getTaskEndpoint().clearTasks(session);
        @NotNull final SessionDTO adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
        bootstrap.getSessionEndpoint().closeSession(adminSession);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateFindRemove() {
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task CFR");
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task CFR 2");
        bootstrap.getTaskEndpoint().createTaskWithDescription(session, "AUTO Task CFR 3", "Desc");
        @NotNull List<TaskDTO> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(3, listTasks.size());

        @NotNull String taskId = listTasks.get(0).getId();
        @NotNull TaskDTO taskById = bootstrap.getTaskEndpoint().findTaskById(session, taskId);
        Assert.assertEquals(listTasks.get(0).getName(), taskById.getName());
        @NotNull TaskDTO taskByIndex = bootstrap.getTaskEndpoint().findTaskByIndex(session, 2);
        Assert.assertEquals(listTasks.get(1).getName(), taskByIndex.getName());
        @NotNull TaskDTO taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task CFR 3");
        Assert.assertEquals("AUTO Task CFR 3", taskByName.getName());

        taskId = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task CFR").getId();
        bootstrap.getTaskEndpoint().removeTaskById(session, taskId);
        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task CFR 3");
        bootstrap.getTaskEndpoint().removeTaskByIndex(session, 1);
        listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdate() {
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Update");

        @NotNull TaskDTO task = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Update");
        @NotNull final String taskId = task.getId();
        bootstrap.getTaskEndpoint().updateTaskById(session, taskId, "AUTO Update", "DESC");
        task = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Update");
        Assert.assertEquals("DESC", task.getDescription());

        bootstrap.getTaskEndpoint().updateTaskByIndex(session, 1, "JUNIT Update", "JDESC");
        task = bootstrap.getTaskEndpoint().findTaskByName(session, "JUNIT Update");
        Assert.assertEquals("JDESC", task.getDescription());

        bootstrap.getTaskEndpoint().removeTaskByName(session, "JUNIT Update");
        @NotNull final List<TaskDTO> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatus() {
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Status");
        bootstrap.getTaskEndpoint().createTaskWithDescription(session, "AUTO Task Status 2", "Desc");

        @NotNull TaskDTO taskById = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status");
        @NotNull final String taskId = taskById.getId();
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
        bootstrap.getTaskEndpoint().startTaskById(session, taskId);
        taskById = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status");
        Assert.assertEquals(Status.IN_PROGRESS, taskById.getStatus());
        bootstrap.getTaskEndpoint().finishTaskById(session, taskId);
        taskById = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status");
        Assert.assertEquals(Status.COMPLETED, taskById.getStatus());
        bootstrap.getTaskEndpoint().changeTaskStatusById(session, taskId, Status.NOT_STARTED);
        taskById = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status");
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());

        @NotNull TaskDTO taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status 2");
        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());
        bootstrap.getTaskEndpoint().startTaskByName(session, "AUTO Task Status 2");
        taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status 2");
        Assert.assertEquals(Status.IN_PROGRESS, taskByName.getStatus());
        bootstrap.getTaskEndpoint().finishTaskByName(session, "AUTO Task Status 2");
        taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status 2");
        Assert.assertEquals(Status.COMPLETED, taskByName.getStatus());
        bootstrap.getTaskEndpoint().changeTaskStatusByName(session, "AUTO Task Status 2", Status.NOT_STARTED);
        taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Status 2");
        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());

        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task Status");
        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task Status 2");
        @NotNull final List<TaskDTO> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testBindUnbindTask() {
        bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        @NotNull final String projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Binding");
        @NotNull TaskDTO taskFirst = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Binding");
        bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Binding 2");
        @NotNull TaskDTO taskSecond = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task Binding 2");

        bootstrap.getTaskEndpoint().bindTaskByProjectId(session, projectId, taskFirst.getId());
        bootstrap.getTaskEndpoint().bindTaskByProjectId(session, projectId, taskSecond.getId());
        @NotNull List<TaskDTO> tasks = bootstrap.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        Assert.assertEquals(2, tasks.size());

        bootstrap.getTaskEndpoint().unbindTaskByProjectId(session, taskSecond.getId());
        tasks = bootstrap.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        Assert.assertEquals(1, tasks.size());

        bootstrap.getTaskEndpoint().removeTaskById(session, taskFirst.getId());
        bootstrap.getTaskEndpoint().removeTaskById(session, taskSecond.getId());
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        @NotNull final List<TaskDTO> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

}
