package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(@NotNull final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(@NotNull final String value) {
        super("Error! This value '" + value + "' is not a number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
