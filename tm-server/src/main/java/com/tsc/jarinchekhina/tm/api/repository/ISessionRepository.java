package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public interface ISessionRepository {

    void addAll(@NotNull Collection<SessionDTO> collection);

    void update(@NotNull SessionDTO session);

    void clear();

    void remove(@NotNull SessionDTO session);

    void removeById(@NotNull String id);

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    List<SessionDTO> findByUserId(@NotNull String userId);

    @NotNull
    SessionDTO findById(@NotNull String id);

}
