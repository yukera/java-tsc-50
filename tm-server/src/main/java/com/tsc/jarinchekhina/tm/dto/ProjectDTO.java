package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.api.entity.IWBS;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.listener.EntityDTOListener;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_project")
@EntityListeners(EntityDTOListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectDTO extends AbstractEntityDTO implements IWBS {

    public ProjectDTO(@Nullable final Project project) {
        this.setId(project.getId());
        @Nullable final User user = project.getUser();
        if (user != null) this.userId = user.getId();
        this.name = project.getName();
        this.description = project.getDescription();
        this.status = project.getStatus();
        this.dateStart = project.getDateStart();
        this.dateFinish = project.getDateFinish();
        this.created = project.getCreated();
    }

    @Column(name = "user_id")
    @NotNull
    private String userId;

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }

}
