package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void add(@NotNull final Task task) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<Task> collection) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.addAll(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final Task task) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = new Task();
            task.setName(name);
            taskRepository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final Task task = new Task();
            task.setName(name);
            task.setDescription(description);
            taskRepository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@Nullable final String userId, final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final Task task) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeById(userId, task.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            @Nullable final Task task = taskRepository.findByIndex(userId, index);
            taskRepository.removeById(userId, task.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = findById(userId, id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void changeTaskStatus(
            @NotNull final String userId,
            @NotNull final Task task,
            @NotNull final Status status
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            task.setStatus(status);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findById(userId, id);
        changeTaskStatus(userId, task, status);
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByIndex(userId, index);
        changeTaskStatus(userId, task, status);
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByName(userId, name);
        changeTaskStatus(userId, task, status);
    }

    @Override
    @SneakyThrows
    public void startTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    public void startTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    public void startTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    public void finishTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    public void finishTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    public void finishTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.COMPLETED);
    }

}
