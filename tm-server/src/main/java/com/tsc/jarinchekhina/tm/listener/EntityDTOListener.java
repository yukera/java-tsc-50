package com.tsc.jarinchekhina.tm.listener;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.dto.AbstractEntityDTO;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

@NoArgsConstructor
public final class EntityDTOListener<E extends AbstractEntityDTO> {

    @PrePersist
    @SneakyThrows
    public void prePersist(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.PRE_PERSIST);
    }

    @PostPersist
    @SneakyThrows
    public void postPersist(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.POST_PERSIST);
    }

    @PreRemove
    @SneakyThrows
    public void preRemove(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.PRE_REMOVE);
    }

    @PostRemove
    @SneakyThrows
    public void postRemove(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.POST_REMOVE);
    }

    @PreUpdate
    @SneakyThrows
    public void preUpdate(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.PRE_UPDATE);
    }

    @PostUpdate
    @SneakyThrows
    public void postUpdate(@NotNull final E entity) {
        Bootstrap.sendMessage(entity, EntityActionType.POST_UPDATE);
    }

}
