package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * EntityLogDTO
 *
 * @author Yuliya Arinchekhina
 */
@Getter
@Setter
@Builder
@RequiredArgsConstructor
public class EntityLogDTO {

    private final String id = UUID.randomUUID().toString();

    private final String formattedDate;

    private final String className;

    private final EntityActionType actionType;

    private final Object entityDTO;

    private final String tableName;

}
