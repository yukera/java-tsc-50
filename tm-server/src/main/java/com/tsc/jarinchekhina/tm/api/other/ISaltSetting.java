package com.tsc.jarinchekhina.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getSessionIteration();

    @NotNull
    String getSessionSecret();

}
