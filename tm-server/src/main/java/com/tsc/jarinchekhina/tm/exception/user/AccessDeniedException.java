package com.tsc.jarinchekhina.tm.exception.user;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
